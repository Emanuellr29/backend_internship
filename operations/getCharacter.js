const axios = require("axios");

function getCharacter(id = null) {
  return new Promise((resolve, reject) => {
    if (id === null || id === undefined)
      reject(new Error("id null or undefined"));

    axios
      .get(`https://rickandmortyapi.com/api/character/${id}`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((err) => {
        reject(`${err}`);
      });
  });
}

module.exports = {
  getCharacter: getCharacter,
};
